//Screen 11
package com.cognizant.iot.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.utils.OptionsMenuActivity;
import com.cognizant.retailmate.R;

//import android.widget.Toast;

public class OrderDetail extends OptionsMenuActivity {

	int passedvar;
	private TextView passtext = null;

	// private TextView offerdesc=null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen11);

		// (MainActivity.positon);
		Intent myIntent = getIntent();
		passtext = (TextView) findViewById(R.id.order);
		passtext.setText("ORDER :" + (myIntent.getStringExtra("position")));
		Toast.makeText(getBaseContext(),
				"Is selected " + myIntent.getStringExtra("position"),
				Toast.LENGTH_LONG).show();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_old, menu);
		return true;
	}

}
