package com.cognizant.iot.activity;

import java.util.List;
import java.util.Vector;

import android.content.res.Resources;

import com.cognizant.retailmate.R;

//ShoppingCartHelper class is designed to hold information about all the products in the catalog
// and all the products that are in the user's shopping cart / wish list .
public class ShoppingWishlistHelper {
	public static final String PRODUCT_INDEX = "PRODUCT_INDEX";
	private static List<Product> catalog;
	private static List<Product> cart;

	public static List<Product> getCatalog(Resources res) {
		if (catalog == null) {
			catalog = new Vector<Product>();
			catalog.add(new Product("Dead or Alive", res
					.getDrawable(R.drawable.deadoralive),
					"Dead or Alive by Tom Clancy with Grant Blackwood", 1250));
			catalog.add(new Product("Switch", res
					.getDrawable(R.drawable.switchbook),
					"Switch by Chip Heath and Dan Heath", 400));
			catalog.add(new Product("Watchmen", res
					.getDrawable(R.drawable.watchmen),
					"Watchmen by Alan Moore and Dave Gibbons", 15000));
			catalog.add(new Product("Dead or Alive", res
					.getDrawable(R.drawable.deadoralive),
					"Dead or Alive by Tom Clancy with Grant Blackwood", 1250));
			catalog.add(new Product("Switch", res
					.getDrawable(R.drawable.switchbook),
					"Switch by Chip Heath and Dan Heath", 400));
			catalog.add(new Product("Watchmen", res
					.getDrawable(R.drawable.watchmen),
					"Watchmen by Alan Moore and Dave Gibbons", 15000));
			catalog.add(new Product("Dead or Alive", res
					.getDrawable(R.drawable.deadoralive),
					"Dead or Alive by Tom Clancy with Grant Blackwood", 1250));
			catalog.add(new Product("Switch", res
					.getDrawable(R.drawable.switchbook),
					"Switch by Chip Heath and Dan Heath", 400));
			catalog.add(new Product("Watchmen", res
					.getDrawable(R.drawable.watchmen),
					"Watchmen by Alan Moore and Dave Gibbons", 15000));
			catalog.add(new Product("Dead or Alive", res
					.getDrawable(R.drawable.deadoralive),
					"Dead or Alive by Tom Clancy with Grant Blackwood", 1250));
			catalog.add(new Product("Switch", res
					.getDrawable(R.drawable.switchbook),
					"Switch by Chip Heath and Dan Heath", 400));
			catalog.add(new Product("Watchmen", res
					.getDrawable(R.drawable.watchmen),
					"Watchmen by Alan Moore and Dave Gibbons", 15000));
		}
		return catalog;
	}

	public static List<Product> getCart() {
		if (cart == null) {
			cart = new Vector<Product>();
		}
		return cart;
	}
}