package com.cognizant.iot.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.utils.OptionsMenuActivity;
import com.cognizant.retailmate.R;

/**
 * Created by 452016 on 4/17/2015.
 */
public class Share_wishlist extends OptionsMenuActivity {

	TextView sender, receiver;
	Button share;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_wishlist);

		sender = (TextView) findViewById(R.id.sender);
		receiver = (TextView) findViewById(R.id.receiver);
		share = (Button) findViewById(R.id.send_button);

		share.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				Toast.makeText(getApplicationContext(), "Shared Successfully",
						Toast.LENGTH_LONG).show();
			}
		});

	}
}
