package com.cognizant.iot.activity;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.cognizant.iot.utils.OptionsMenuActivity;
import com.cognizant.retailmate.R;

public class ShoppingWishlistActivity extends OptionsMenuActivity {
	Intent sharewishlist_intent;
	private List<Product> mCartList;
	private ProductAdapter mProductAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.my_wishlist);

		mCartList = ShoppingWishlistHelper.getCart();

		// Make sure to clear the selections
		for (int i = 0; i < mCartList.size(); i++) {
			mCartList.get(i).selected = false;
		}

		// Create the list
		final ListView listViewCatalog = (ListView) findViewById(R.id.ListViewCatalog);
		mProductAdapter = new ProductAdapter(mCartList, getLayoutInflater(),
				true, true); // to show checkbox and textbox at the wishlist or
								// nort ??
		listViewCatalog.setAdapter(mProductAdapter);

		listViewCatalog.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Product selectedProduct = mCartList.get(position);

				if (selectedProduct.selected == true)
					selectedProduct.selected = false;
				else
					selectedProduct.selected = true;

				mProductAdapter.notifyDataSetInvalidated();

			}
		}); // onClick ends

		Button removeButton = (Button) findViewById(R.id.ButtonRemoveFromCart);
		removeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Loop through and remove all the products that are selected
				// Loop backwards so that the remove works correctly
				for (int i = mCartList.size() - 1; i >= 0; i--) {

					if (mCartList.get(i).selected) {
						mCartList.remove(i);
					}
				}
				mProductAdapter.notifyDataSetChanged();
			}
		});

		Button sharewishlist_btn = (Button) findViewById(R.id.sharewishlist_button);

		sharewishlist_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				sharewishlist_intent = new Intent(
						ShoppingWishlistActivity.this, Share_wishlist.class);
				startActivity(sharewishlist_intent);

			}
		});

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		finish();
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_old, menu);
		return true;
	}
}
