package com.cognizant.iot.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.utils.OptionsMenuActivity;
import com.cognizant.retailmate.R;

public class Product_details_FindProduct extends OptionsMenuActivity {

	public String value = "";
	// int id;
	Intent myIntent = getIntent();
	TextView name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_details_findproduct);

		/* id=myIntent.getIntExtra("ID", -1); */

		/*
		 * res=myIntent.getStringExtra("ProductName");
		 * 
		 * //res=myIntent.("Product Name");
		 * Toast.makeText(getApplicationContext(), res,
		 * Toast.LENGTH_SHORT).show(); TextView name=(TextView)
		 * findViewById(R.id.product_details_findProduct); name.setText(res);
		 */

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			value = extras.getString("ProductName");
			Toast.makeText(getApplicationContext(), value, Toast.LENGTH_SHORT)
					.show();

			name = (TextView) findViewById(R.id.product_details_findProduct_textView);
			name.setText(value);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_old, menu);
		return true;
	}

}
