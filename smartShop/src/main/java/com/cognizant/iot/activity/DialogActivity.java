package com.cognizant.iot.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.model.CustomerMessageModel;
import com.cognizant.retailmate.R;


/**
 * Created by 452781 on 12/12/2016.
 */
public class DialogActivity extends AppCompatActivity {

    final float growTo = .1f;
    final long duration = 500;
    ImageView imageView;
    TextView greeting, location, title, last_visit, loyalty;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        ScaleAnimation grow = new ScaleAnimation(1, growTo, 1, growTo,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        grow.setDuration(duration / 2);
        ScaleAnimation shrink = new ScaleAnimation(growTo, 1, growTo, 1,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(duration / 2);
        shrink.setStartOffset(duration / 2);
        AnimationSet growAndShrink = new AnimationSet(true);
        growAndShrink.setInterpolator(new LinearInterpolator());
        growAndShrink.addAnimation(shrink);
        imageView = (ImageView) findViewById(R.id.imageViewPic);
        imageView.startAnimation(growAndShrink);

        greeting = (TextView) findViewById(R.id.greet_name);
        location = (TextView) findViewById(R.id.location);
        title = (TextView) findViewById(R.id.title);
        last_visit = (TextView) findViewById(R.id.last_visit);
        loyalty = (TextView) findViewById(R.id.loyalty);

        String flag = getIntent().getStringExtra("type");

        if (flag.equals("greeting")) {

            CustomerMessageModel customerMessageModel = (CustomerMessageModel) getIntent().getSerializableExtra("customerModel");

            title.setText("Welcome!");
            greeting.setText(customerMessageModel.getCustomerName());
            StringBuilder sb = new StringBuilder();
            sb.append("Here are some products you might be interested in:"+"\n");
            for(int i=0;i<customerMessageModel.getProducts().size();i++)
                sb.append("["+i+".]"+customerMessageModel.getProducts().get(i)+"\n");
            location.setText(sb);
            last_visit.setText(customerMessageModel.getDays() + " Days");
            loyalty.setText(String.valueOf(customerMessageModel.getLoyaltyPoints()));

        } else if (flag.equals("geo")) {

            title.setText("Geofence Alert");
            greeting.setText(getIntent().getStringExtra("name"));
            location.setText("is less than " + getIntent().getStringExtra("dist") + " from the store.");

        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.MyDatePickerDialogTheme)).create();
            alertDialog.setTitle("Order Update!");
            alertDialog.setMessage("Check your order status!");
            // Alert dialog button
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Not right now",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Alert dialog action goes here
                            // onClick button code here
                            dialog.dismiss();// use dismiss to cancel alert dialog
                            finish();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getApplicationContext(), CatalogActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
            alertDialog.show();

        }
    }

    void closeFragment(View view) {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
