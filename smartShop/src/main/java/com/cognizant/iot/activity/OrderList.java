//Screen 10
package com.cognizant.iot.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.cognizant.iot.utils.OptionsMenuActivity;
import com.cognizant.retailmate.R;

//import android.util.Log;

public class OrderList extends OptionsMenuActivity {

	// public final static String position="com.example.listview.List1";
	int p;
	String arr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen10main);
		final String[] orders = { "OID1432", "OID4332", "OID5564", "OID3213" };
		ListView lw = (ListView) findViewById(R.id.mocklist);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.screen10, R.id.offer_name, orders);
		lw.setAdapter(adapter);

		lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				Intent in = new Intent(OrderList.this, OrderDetail.class);
				p = (int) arg0.getItemIdAtPosition(arg2);// p stores the value
															// of the
				arr = orders[p];
				in.putExtra("position", arr);

				startActivity(in);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_old, menu);
		return true;
	}

}
