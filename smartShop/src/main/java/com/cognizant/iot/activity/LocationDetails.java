package com.cognizant.iot.activity;

public class LocationDetails 
{
	public String locationName;
	public double latitude;
	public double longitude;
	public static int count=0;
	
	static public void setCount(int num)
	{
		count=num;
	}
	
	
	public double getLatitude() 
	{
		return latitude;
	}
	
	
	public double getLongitude() 
	{
		return longitude;
	}
	
	
	public String getLocationName() 
	{
		return locationName;
	}
	
	
	public void setLatitude(double latitude) 
	{
		this.latitude = latitude;
	}
	
	
	public void setLocationName(String locationName) 
	{
		this.locationName = locationName;
	}
	
	
	public void setLongitude(double longitude) 
	{
		this.longitude = longitude;
	}
}
