package com.cognizant.iot.utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cognizant.iot.json.AssetsExtracter;
import com.cognizant.retailmate.R;

public class Login_Activity extends Activity {

	Button btn;
	EditText usrn, passw;
	AssetsExtracter mTask;
	private NotificationManager notificationManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		notificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		// set up notitle
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// set up full screen
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.login_layout);

		btn = (Button) findViewById(R.id.submit);
		usrn = (EditText) findViewById(R.id.username);
		passw = (EditText) findViewById(R.id.password);

		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (usrn.getText().toString().equals("admin")
						&& passw.getText().toString().equals("admin")) {
					mTask = new AssetsExtracter("empty", Login_Activity.this);
					mTask.execute();

				} else if (usrn.getText().toString().equals("")
						|| passw.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(),
							"Username or Password should not empty",
							Toast.LENGTH_SHORT).show();
				} else {
					usrn.setText("");
					passw.setText("");
					Toast.makeText(getApplicationContext(),
							"Invalid credentials", Toast.LENGTH_SHORT).show();
				}

			}
		});

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		notificationManager.cancelAll();
		super.onDestroy();

	}
}
