package com.cognizant.iot.insidestore;

/**
 * Created by 452781 on 12/6/2016.
 */
public class AssociateDetailsModel {


    private String EMP_ID;

    private String EMP_NAME;


    public String getAssociateId() {
        return EMP_ID;
    }

    public void setAssociateId(String AssociateId) {
        this.EMP_ID = AssociateId;
    }


    public String getAssociateName() {
        return EMP_NAME;
    }

    public void setAssociateName(String AssociateName) {
        this.EMP_NAME = AssociateName;
    }

}