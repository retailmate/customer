package com.cognizant.iot.activitywishlist;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.utils.MySSLSocketFactory;

import android.util.Log;
 
public class JSONfunctions_wish {
 
	public static JSONObject getJSONfromURL(String url) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;
		ArrayList<NameValuePair> postParameters;
 
		// Download JSON data from URL
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			
		      List<NameValuePair> params = new ArrayList<NameValuePair>();
	            params.add(new BasicNameValuePair("macaddr", CatalogActivity.imei));
	            //params.add(new BasicNameValuePair("pass", "xyz"));
	            //params.add(new BasicNameValuePair("pass", "xyz"));
	            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,HTTP.UTF_8);
	            httppost.setEntity(ent);
	          
			
			/*
			httppost.setHeader("Content-Type", "application/json");
			HttpEntity entity = new StringEntity("{\"macaddr\":\" 000000000000788 \"}");
			httppost.setEntity(entity);
			*/
			
			
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();
 
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}
 
		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}
 
		try {
 
			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}
 
		return jArray;
	}
	
	public static JSONObject getJSONfromAXURLGETWishlist(String url) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;

		// Download JSON data from URL
		try {

			HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("Content-type", "application/json");

			//httpget.addHeader("Authorization", CatalogActivity.access_token_AX);
			//httpget.addHeader("Authorization", CatalogActivity.google_token);
			//httpget.addHeader("OUN","052");

			HttpResponse response = httpclient.execute(httpPost);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
			Log.d("@@##", "wishlist result " + result);
		} catch (Exception e) {
			Log.e("log_tag", "wishlist Error converting result " + e.toString());
		}

		try {

			jArray = new JSONObject(result);
			Log.d("@@##", "wishlist jArray " + jArray);
		} catch (JSONException e) {
			Log.e("log_tag", "wishlist Error parsing data " + e.toString());
		}

		return jArray;
	}
}
