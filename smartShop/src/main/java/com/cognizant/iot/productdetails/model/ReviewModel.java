package com.cognizant.iot.productdetails.model;

import android.graphics.drawable.Drawable;

/**
 * Created by 599584 on 2/21/2017.
 */

public class ReviewModel {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    String name,date,review;
    Drawable image;
}
