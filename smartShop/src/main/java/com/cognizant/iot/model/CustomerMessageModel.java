package com.cognizant.iot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 452781 on 7/7/2017.
 */
public class CustomerMessageModel implements Serializable {
    @SerializedName("CustomerId")
    @Expose
    private String customerId;
    @SerializedName("NotificationMessage")
    @Expose
    private String notificationMessage;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("Days")
    @Expose
    private Integer days;
    @SerializedName("LoyaltyPoints")
    @Expose
    private Integer loyaltyPoints;
    @SerializedName("Products")
    @Expose
    private List<Object> products = null;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public List<Object> getProducts() {
        return products;
    }

    public void setProducts(List<Object> products) {
        this.products = products;
    }
}
