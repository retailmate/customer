package com.cognizant.iot.model;

import java.io.Serializable;

public class BrandModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */

    String Name;
    Boolean VariantExists;
    String Code;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Boolean getVariantExists() {
        return VariantExists;
    }

    public void setVariantExists(Boolean variantExists) {
        VariantExists = variantExists;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

}
