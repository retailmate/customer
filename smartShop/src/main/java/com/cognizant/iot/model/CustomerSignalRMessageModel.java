package com.cognizant.iot.model;

import java.util.List;

/**
 * Created by 452781 on 7/7/2017.
 */
public class CustomerSignalRMessageModel {

    private String H;
    private String M;
    private List<String> A;

    public String getH() {
        return H;
    }

    public void setH(String H) {
        this.H = H;
    }

    public String getM() {
        return M;
    }

    public void setM(String M) {
        this.M = M;
    }

    public List<String> getA() {
        return A;
    }

    public void setA(List<String> A) {
        this.A = A;
    }

}
