package com.cognizant.iot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 452781 on 1/24/2017.
 */
public class CustomerLocationModel {

    @SerializedName("CustomerId")
    @Expose
    private Integer customerId;
    @SerializedName("AssociateId")
    @Expose
    private Integer associateId;
    @SerializedName("BeaconId")
    @Expose
    private String beaconId;
    @SerializedName("CaptureTime")
    @Expose
    private String captureTime;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getAssociateId() {
        return associateId;
    }

    public void setAssociateId(Integer associateId) {
        this.associateId = associateId;
    }

    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public String getCaptureTime() {
        return captureTime;
    }

    public void setCaptureTime(String captureTime) {
        this.captureTime = captureTime;
    }


}
