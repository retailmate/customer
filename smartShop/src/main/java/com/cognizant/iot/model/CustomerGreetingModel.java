package com.cognizant.iot.model;

/**
 * Created by 452781 on 1/24/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerGreetingModel {

    @SerializedName("CustomerId")
    @Expose
    private Integer customerId;
    @SerializedName("AssociateId")
    @Expose
    private Integer associateId;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("CaptureTime")
    @Expose
    private String captureTime;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getAssociateId() {
        return associateId;
    }

    public void setAssociateId(Integer associateId) {
        this.associateId = associateId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaptureTime() {
        return captureTime;
    }

    public void setCaptureTime(String captureTime) {
        this.captureTime = captureTime;
    }


}
