package com.cognizant.iot.chatbot;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageButton;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guest_User on 22/11/16.
 */

public class ChatGlobal {
    /**
     * Refactored searchAPIToken to googleToken
     */

    public ChatGlobal(Context context) {
        this.mContext = context;
    }

    public static final int SEND = 0;
    public static final int RECEIVE = 1;
    public static final int RECEIVE_PRODUCT = 2;
    public static final int RECEIVE_OFFER = 3;
    public static final int RECEIVE_SEARCH = 4;
    public static final int RECEIVE_SUGGEST = 5;
    // Base url for Luis
    public static String baseURL = "https://api.projectoxford.ai/luis/v2.0/apps/8238b8fc-90be-4121-a857-4617b357ffa9?subscription-key=28eede3a83c34c9ab060653664e8d1b9&q=";
    //token for search api
    public static String googleToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImI5N2FmM2E4OTk4MzJmNTU1ODAzOWQ1NDg4MzY0ZDEyOWViNmE3YjcifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJpYXQiOjE0ODI4NDA3NTMsImV4cCI6MTQ4Mjg0NDM1MywiYXVkIjoiNjU2NTUxODMwMDYwLXVpaWptb3A3YWxvbGR1NjdlbGlpOXZoaGM4cWNuazR0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTE3NTYzNjc1MDA4NDE4MDA2NTA3IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF6cCI6IjY1NjU1MTgzMDA2MC1odjM2ZWthdXFrNzFkaW40bjVqNTcxcGRnMzFodHU5OS5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImVtYWlsIjoicmV0YWlsbWF0ZWF4QGdtYWlsLmNvbSIsIm5hbWUiOiJBbWVyIEhhc2FuIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS8tMjkwbUZGX1lIaUkvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQUEvQUtCX1U4c3VkTFNzdmt5a3I0bVNya2lsRUVMMFo0ZHJBUS9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoiQW1lciIsImZhbWlseV9uYW1lIjoiSGFzYW4iLCJsb2NhbGUiOiJlbiJ9.VNmKyAlfeB813Um3rWRHRppE-7LLPzZZ0P2nzqYZVSrWvOFE5zhK8ugwMRbWJWuRCeiJuiAD6J_qlWTbHHmRZZ56BF0Uy1jiJOdiSe4p_8pqTdL3jpVvIgrBDJJ3d5oOuMdHndoD3d6Dc3T2CvX8uRc4v_0H_hLlWjFp0TLmhzXTmizAkPSjUI8fZEw6YkhrhnO803LXY5Y8STAroBsxUjYtSGNGA8KVGOXBPTfCe6YM1Ba3724fcAUnvg8sOsOLVw_Rl__KCgHKkj0EdH735Ke_mdqgE6DfxX8NdoBg_Ga-4kNy0eXulVBaUC_tojEzYUkTyj_frNDQ-udMgA-rMw";

    //for YES/NO(means optional) response
    public static boolean isAsked = false;
    public static boolean askedSpecific = false;
    public static boolean intentIsRecommend = false;
    public static boolean intentIsAvailable = false;
    public static boolean intentIsPrice = false;
    public static boolean intentIsAddToCart = false;
    public static boolean isaddToCartAPI = false;
    //for mandatory response
    public static boolean isPrompt = false;
    public static String imageBaseUrl = "https://jda1devret.cloudax.dynamics.com/MediaServer/";
    public static String recommendAPI = "http://jdaretailmateapidev.azurewebsites.net/api/CognitiveServices/GetUserToItemRecommendationsAPI";
    public static String baseAddToCartAPI = "https://jdacustomervalidation.azurewebsites.net/api/cart/CreateCart?idToken=";
    public static String baseProductSearchAPI = "https://jda1devret.cloudax.dynamics.com/Commerce/Products/SearchByText(channelId=5637144592,catalogId=0,searchText=%27";
    public static boolean tapToAdd = false;
    public static StringBuffer cartBuffer = new StringBuffer();
    public static String productID = "";
    public static String userId = "004021";

    public static TextToSpeech textToSpeech;
    public static int result;
    public static Context mContext;
    public static boolean online = true;

    //FOR GOOGLE SEARCH IN DEFAULT CASE.
    //https://www.googleapis.com/customsearch/v1?q=do+you+have+dell+laptop&cx=006896418020272592208%3Agznxrju2_ha&filter=1&num=5&safe=medium&key=AIzaSyAPXvMXt1MR5PMYMH7hoi2Pvfx2deHz6TQ
    public static String baseSearchUrl = "https://www.googleapis.com/customsearch/v1?q=";
    public static String endSearchUrl = "&cx=006896418020272592208%3Agznxrju2_ha&filter=1&num=5&safe=medium&key=AIzaSyAPXvMXt1MR5PMYMH7hoi2Pvfx2deHz6TQ";
    public static boolean hasSearchImage = false;

    //Intent specific Info
    public static String contextId;
    public static String lastIntent = "";

    //Layout Components
    public static RecyclerView mRecyclerView;
    public static ChatAdapter mAdapter;
    public static EditText chatText;
    public static ImageButton sendButton;
    public static RecyclerView.LayoutManager mLayoutManager;

    public static List<String> mDataset;
    public static ArrayList<Integer> mDatasetTypes;

    public static List<String> entityItemList = new ArrayList<>();
    public static List<String> entityBooleanList = new ArrayList<>();
    public static List<Integer> entityNumberList = new ArrayList<>();
    public static List<String> itemMatchedList = new ArrayList<>();
    public static List<String> itemMatchedIdList = new ArrayList<>();
    public static List<ProductDataModel> productList = new ArrayList<>();
    public static List<ProductDataModel> prevProductList = new ArrayList<>();
    public static List<SuggestDataModel> suggestDataModels;
    public static List<ChatDataModel> chatDataModels;

    public static int countPrompt = 0;


}
