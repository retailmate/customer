package com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltymodel.LoyaltyResponseModel;
import com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyrecycview.HistoryAdapter;
import com.cognizant.retailmate.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 543898 on 1/23/2017.
 */
public class HistoryFragment extends Fragment {


    RecyclerView recyclerView;

    Gson gson;

    LoyaltyResponseModel loyaltyResponseModel;

    HistoryAdapter recyclerView_Adapter;

    RecyclerView.LayoutManager recyclerViewLayoutManager;

    Context mCxt;

    public HistoryFragment(Context context) {
        // Required empty public constructor
        mCxt = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gson = new Gson();

        loyaltyResponseModel = gson.fromJson(loadJSONFromAsset(), LoyaltyResponseModel.class);
//        Log.e("#### loyaltymodel", loyaltyResponseModel.toString() + "__");
//        Log.e("#### loyaltymodel", loyaltyResponseModel.getReedemptionPoint().toString() + "__");
//        Log.e("#### loyaltymodel", loyaltyResponseModel.getTransactionDetail().toString() + "__");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View historyLayout = inflater.inflate(R.layout.loyalty_multiple_history, container, false);


        recyclerView = (RecyclerView) historyLayout.findViewById(R.id.recycler_view2);

        recyclerViewLayoutManager = new GridLayoutManager(historyLayout.getContext(), 1);

        recyclerView.setLayoutManager(recyclerViewLayoutManager);

        Log.e("####", loyaltyResponseModel.getTransactionDetail().size() + "__");

        recyclerView_Adapter = new HistoryAdapter(mCxt, loyaltyResponseModel);

        recyclerView.setAdapter(recyclerView_Adapter);

        return historyLayout;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getApplicationContext().getAssets().open("loyaltyoffline.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("#### json", json + "__");
        return json;
    }


}
