package com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyrecycview;

/**
 * Created by 543898 on 12/26/2016.
 */

public class LoyaltyCards {
    String rewards;

    public Integer getRewardsBrand() {
        return rewardsBrand;
    }

    public void setRewardsBrand(Integer rewardsBrand) {
        this.rewardsBrand = rewardsBrand;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    Integer rewardsBrand;
}
