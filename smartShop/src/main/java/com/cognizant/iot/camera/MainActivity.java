package com.cognizant.iot.camera;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.cognizant.retailmate.R;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 001;
    private static final int MY_PERMISSIONS_REQUEST_EXT_STORAGE = 007;
    ImageView iv;

    Context con;

    ImageView click_pic, share;

    ImageView instaView, skypeView, hangoutsView;

    boolean flag = false;

    Bitmap bmp;

    Intent starterIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        starterIntent = getIntent();

        byte[] byteArray = getIntent().getByteArrayExtra("image");
        bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

//        ImageView imageView= (ImageView) findViewById(R.id.imageView);
//        imageView.setImageBitmap(bmp);


        int permissionCheckCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        int permissionCheckData = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        Log.e("TAG", "*************************" + permissionCheckCamera + "***************************" + permissionCheckData);


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_selfie);

        iv = (ImageView) findViewById(R.id.imageView1);
        iv.setImageBitmap(bmp);

        click_pic = (ImageView) findViewById(R.id.click_pic);
        share = (ImageView) findViewById(R.id.share);

        instaView = (ImageView) findViewById(R.id.insta);
        hangoutsView = (ImageView) findViewById(R.id.hangouts);
        skypeView = (ImageView) findViewById(R.id.skype);


        instaView.setVisibility(View.INVISIBLE);
        hangoutsView.setVisibility(View.INVISIBLE);
        skypeView.setVisibility(View.INVISIBLE);


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag == false) {
                    instaView.setVisibility(View.VISIBLE);
                    hangoutsView.setVisibility(View.VISIBLE);
                    skypeView.setVisibility(View.VISIBLE);
                    flag = true;
                } else if (flag == true) {
                    instaView.setVisibility(View.INVISIBLE);
                    hangoutsView.setVisibility(View.INVISIBLE);
                    skypeView.setVisibility(View.INVISIBLE);
                    flag = false;

                }

            }
        });

        con = this;


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finish();
                    startActivity(starterIntent);
                } else {
                    finish();
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_EXT_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finish();
                    startActivity(starterIntent);
                } else {
                    finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.click_pic:
                finish();

                Intent cameraIntent = new Intent();
                cameraIntent.setClass(this, CameraActivity.class);
                startActivity(cameraIntent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent cameraIntent = new Intent();
        cameraIntent.setClass(this, CameraActivity.class);
        startActivity(cameraIntent);


    }

    @Override
    protected void onResume() {
        super.onResume();

        instaView.setVisibility(View.INVISIBLE);
        hangoutsView.setVisibility(View.INVISIBLE);
        skypeView.setVisibility(View.INVISIBLE);
    }


    public void hangoutsIntent(View view) {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setPackage("com.google.android.talk");
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Hi!");
        try {
            startActivity(sendIntent);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.google.android.talk")));
        }

    }

    public void instagramIntent(View view) {
        Intent instagram = new Intent(Intent.ACTION_SEND);
        instagram.setType("image/*");
        instagram.putExtra(Intent.EXTRA_STREAM, bitmapToUri(getApplicationContext(), bmp));
        instagram.setPackage("com.instagram.android");
        try {
            startActivity(instagram);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.instagram.android")));
        }
    }

    public void skypeIntent(View view) {

        PackageManager packageManager = getPackageManager();
        Intent skype = packageManager.getLaunchIntentForPackage("com.skype.raider");
        try {
            startActivity(skype);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.skype.raider")));
        }

    }


    public Uri bitmapToUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

}




