package com.cognizant.iot.bopis;

/**
 * Created by 452781 on 11/9/2016.
 */
public class RmStoreModel {
    String name;
    String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
