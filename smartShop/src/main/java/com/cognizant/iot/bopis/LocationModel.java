package com.cognizant.iot.bopis;

import java.util.List;

/**
 * Created by 452781 on 11/9/2016.
 */
public class LocationModel {


    /**
     * place : Houston
     * lat : 29.739549
     * lng : -95.462716
     */

    private List<LocationsBean> locations;

    public List<LocationsBean> getLocations() {
        return locations;
    }

    public void setLocations(List<LocationsBean> locations) {
        this.locations = locations;
    }

    public static class LocationsBean {
        private String place;
        private double lat;
        private double lng;
        private String address;

        public String getPlace() {
            return place;
        }

        public void setPlace(String place) {
            this.place = place;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
