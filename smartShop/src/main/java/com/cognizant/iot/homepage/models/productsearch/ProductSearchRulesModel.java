package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/21/2017.
 */

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSearchRulesModel {

    @SerializedName("HasLinkedProducts")
    @Expose
    private Boolean hasLinkedProducts;
    @SerializedName("IsSerialized")
    @Expose
    private Boolean isSerialized;
    @SerializedName("IsActiveInSalesProcess")
    @Expose
    private Boolean isActiveInSalesProcess;
    @SerializedName("IsBlocked")
    @Expose
    private Boolean isBlocked;
    @SerializedName("DateOfBlocking")
    @Expose
    private String dateOfBlocking;
    @SerializedName("DateToActivate")
    @Expose
    private String dateToActivate;
    @SerializedName("DateToBlock")
    @Expose
    private String dateToBlock;
    @SerializedName("DefaultUnitOfMeasure")
    @Expose
    private String defaultUnitOfMeasure;
    @SerializedName("PriceKeyingRequirementValue")
    @Expose
    private Integer priceKeyingRequirementValue;
    @SerializedName("QuantityKeyingRequirementValue")
    @Expose
    private Integer quantityKeyingRequirementValue;
    @SerializedName("MustKeyInComment")
    @Expose
    private Boolean mustKeyInComment;
    @SerializedName("CanQuantityBecomeNegative")
    @Expose
    private Boolean canQuantityBecomeNegative;
    @SerializedName("MustScaleItem")
    @Expose
    private Boolean mustScaleItem;
    @SerializedName("CanPriceBeZero")
    @Expose
    private Boolean canPriceBeZero;
    @SerializedName("ProductId")
    @Expose
    private Integer productId;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public Boolean getHasLinkedProducts() {
        return hasLinkedProducts;
    }

    public void setHasLinkedProducts(Boolean hasLinkedProducts) {
        this.hasLinkedProducts = hasLinkedProducts;
    }

    public Boolean getIsSerialized() {
        return isSerialized;
    }

    public void setIsSerialized(Boolean isSerialized) {
        this.isSerialized = isSerialized;
    }

    public Boolean getIsActiveInSalesProcess() {
        return isActiveInSalesProcess;
    }

    public void setIsActiveInSalesProcess(Boolean isActiveInSalesProcess) {
        this.isActiveInSalesProcess = isActiveInSalesProcess;
    }

    public Boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public String getDateOfBlocking() {
        return dateOfBlocking;
    }

    public void setDateOfBlocking(String dateOfBlocking) {
        this.dateOfBlocking = dateOfBlocking;
    }

    public String getDateToActivate() {
        return dateToActivate;
    }

    public void setDateToActivate(String dateToActivate) {
        this.dateToActivate = dateToActivate;
    }

    public String getDateToBlock() {
        return dateToBlock;
    }

    public void setDateToBlock(String dateToBlock) {
        this.dateToBlock = dateToBlock;
    }

    public String getDefaultUnitOfMeasure() {
        return defaultUnitOfMeasure;
    }

    public void setDefaultUnitOfMeasure(String defaultUnitOfMeasure) {
        this.defaultUnitOfMeasure = defaultUnitOfMeasure;
    }

    public Integer getPriceKeyingRequirementValue() {
        return priceKeyingRequirementValue;
    }

    public void setPriceKeyingRequirementValue(Integer priceKeyingRequirementValue) {
        this.priceKeyingRequirementValue = priceKeyingRequirementValue;
    }

    public Integer getQuantityKeyingRequirementValue() {
        return quantityKeyingRequirementValue;
    }

    public void setQuantityKeyingRequirementValue(Integer quantityKeyingRequirementValue) {
        this.quantityKeyingRequirementValue = quantityKeyingRequirementValue;
    }

    public Boolean getMustKeyInComment() {
        return mustKeyInComment;
    }

    public void setMustKeyInComment(Boolean mustKeyInComment) {
        this.mustKeyInComment = mustKeyInComment;
    }

    public Boolean getCanQuantityBecomeNegative() {
        return canQuantityBecomeNegative;
    }

    public void setCanQuantityBecomeNegative(Boolean canQuantityBecomeNegative) {
        this.canQuantityBecomeNegative = canQuantityBecomeNegative;
    }

    public Boolean getMustScaleItem() {
        return mustScaleItem;
    }

    public void setMustScaleItem(Boolean mustScaleItem) {
        this.mustScaleItem = mustScaleItem;
    }

    public Boolean getCanPriceBeZero() {
        return canPriceBeZero;
    }

    public void setCanPriceBeZero(Boolean canPriceBeZero) {
        this.canPriceBeZero = canPriceBeZero;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}