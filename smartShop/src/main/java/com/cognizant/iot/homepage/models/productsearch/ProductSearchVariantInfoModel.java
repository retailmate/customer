package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/22/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductSearchVariantInfoModel {

    @SerializedName("ActiveVariantProductId")
    @Expose
    private Integer activeVariantProductId;
    @SerializedName("Variants")
    @Expose
    private List<ProductSearchVariantItemModel> variants = null;

    @SerializedName("ItemId")
    @Expose
    private String itemId;
    @SerializedName("MasterProductId")
    @Expose
    private Integer masterProductId;

    public Integer getActiveVariantProductId() {
        return activeVariantProductId;
    }

    public void setActiveVariantProductId(Integer activeVariantProductId) {
        this.activeVariantProductId = activeVariantProductId;
    }

    public List<ProductSearchVariantItemModel> getVariants() {
        return variants;
    }

    public void setVariants(List<ProductSearchVariantItemModel> variants) {
        this.variants = variants;
    }


    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Integer getMasterProductId() {
        return masterProductId;
    }

    public void setMasterProductId(Integer masterProductId) {
        this.masterProductId = masterProductId;
    }

}