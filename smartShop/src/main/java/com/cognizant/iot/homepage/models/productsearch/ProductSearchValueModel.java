package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/21/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductSearchValueModel implements Serializable{

    @SerializedName("RecordId")
    @Expose
    private String recordId;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("ProductNumber")
    @Expose
    private String productNumber;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("IsMasterProduct")
    @Expose
    private Boolean isMasterProduct;
    @SerializedName("IsKit")
    @Expose
    private Boolean isKit;
    @SerializedName("ItemId")
    @Expose
    private String itemId;
    @SerializedName("HasLinkedProducts")
    @Expose
    private Boolean hasLinkedProducts;
    @SerializedName("ProductSearchRulesModel")
    @Expose
    private ProductSearchRulesModel rules;
    @SerializedName("DefaultUnitOfMeasure")
    @Expose
    private Object defaultUnitOfMeasure;
    @SerializedName("Name")
    @Expose
    private Object name;
    @SerializedName("Locale")
    @Expose
    private String locale;
    @SerializedName("OfflineImage")
    @Expose
    private String offlineImage;
    @SerializedName("IsRemote")
    @Expose
    private Boolean isRemote;
    @SerializedName("ProductSearchChangeTrackingInfoModel")
    @Expose
    private ProductSearchChangeTrackingInfoModel changeTrackingInformation;
    @SerializedName("Image")
    @Expose
    private ProductSearchImageModel image;
    @SerializedName("UnitsOfMeasureSymbol")
    @Expose
    private List<Object> unitsOfMeasureSymbol = null;
    @SerializedName("LinkedProducts")
    @Expose
    private List<Object> linkedProducts = null;
    @SerializedName("BasePrice")
    @Expose
    private Double basePrice;
    @SerializedName("Price")
    @Expose
    private Double price;
    @SerializedName("AdjustedPrice")
    @Expose
    private Double adjustedPrice;
    @SerializedName("ProductSearchRetailContextModel")
    @Expose
    private ProductSearchRetailContextModel retailContext;
    @SerializedName("PrimaryCategoryId")
    @Expose
    private Integer primaryCategoryId;
    @SerializedName("CategoryIds")
    @Expose
    private List<Object> categoryIds = null;
    @SerializedName("RelatedProducts")
    @Expose
    private List<ProductSearchRelatedProductModel> relatedProducts = null;
    @SerializedName("ProductsRelatedToThis")
    @Expose
    private List<Object> productsRelatedToThis = null;
    @SerializedName("ProductSchema")
    @Expose
    private List<String> productSchema = null;
    @SerializedName("ProductProperties")
    @Expose
    private List<ProductSearchProductPropertyModel> productProperties = null;
    @SerializedName("CompositionInformation")
    @Expose
    private ProductSearchCompositionInfoModel compositionInformation;
    @SerializedName("ParentKits")
    @Expose
    private List<Object> parentKits = null;
    @SerializedName("SearchName")
    @Expose
    private String searchName;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsMasterProduct() {
        return isMasterProduct;
    }

    public void setIsMasterProduct(Boolean isMasterProduct) {
        this.isMasterProduct = isMasterProduct;
    }

    public Boolean getIsKit() {
        return isKit;
    }

    public void setIsKit(Boolean isKit) {
        this.isKit = isKit;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Boolean getHasLinkedProducts() {
        return hasLinkedProducts;
    }

    public void setHasLinkedProducts(Boolean hasLinkedProducts) {
        this.hasLinkedProducts = hasLinkedProducts;
    }

    public ProductSearchRulesModel getProductSearchRulesModel() {
        return rules;
    }

    public void setProductSearchRulesModel(ProductSearchRulesModel rules) {
        this.rules = rules;
    }

    public Object getDefaultUnitOfMeasure() {
        return defaultUnitOfMeasure;
    }

    public void setDefaultUnitOfMeasure(Object defaultUnitOfMeasure) {
        this.defaultUnitOfMeasure = defaultUnitOfMeasure;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getOfflineImage() {
        return offlineImage;
    }

    public void setOfflineImage(String offlineImage) {
        this.offlineImage = offlineImage;
    }

    public Boolean getIsRemote() {
        return isRemote;
    }

    public void setIsRemote(Boolean isRemote) {
        this.isRemote = isRemote;
    }

    public ProductSearchChangeTrackingInfoModel getProductSearchChangeTrackingInfoModel() {
        return changeTrackingInformation;
    }

    public void setProductSearchChangeTrackingInfoModel(ProductSearchChangeTrackingInfoModel changeTrackingInformation) {
        this.changeTrackingInformation = changeTrackingInformation;
    }

    public ProductSearchImageModel getImage() {
        return image;
    }

    public void setImage(ProductSearchImageModel image) {
        this.image = image;
    }

    public List<Object> getUnitsOfMeasureSymbol() {
        return unitsOfMeasureSymbol;
    }

    public void setUnitsOfMeasureSymbol(List<Object> unitsOfMeasureSymbol) {
        this.unitsOfMeasureSymbol = unitsOfMeasureSymbol;
    }

    public List<Object> getLinkedProducts() {
        return linkedProducts;
    }

    public void setLinkedProducts(List<Object> linkedProducts) {
        this.linkedProducts = linkedProducts;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAdjustedPrice() {
        return adjustedPrice;
    }

    public void setAdjustedPrice(Double adjustedPrice) {
        this.adjustedPrice = adjustedPrice;
    }

    public ProductSearchRetailContextModel getProductSearchRetailContextModel() {
        return retailContext;
    }

    public void setProductSearchRetailContextModel(ProductSearchRetailContextModel retailContext) {
        this.retailContext = retailContext;
    }

    public Integer getPrimaryCategoryId() {
        return primaryCategoryId;
    }

    public void setPrimaryCategoryId(Integer primaryCategoryId) {
        this.primaryCategoryId = primaryCategoryId;
    }

    public List<Object> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<Object> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<ProductSearchRelatedProductModel> getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(List<ProductSearchRelatedProductModel> relatedProducts) {
        this.relatedProducts = relatedProducts;
    }

    public List<Object> getProductsRelatedToThis() {
        return productsRelatedToThis;
    }

    public void setProductsRelatedToThis(List<Object> productsRelatedToThis) {
        this.productsRelatedToThis = productsRelatedToThis;
    }

    public List<String> getProductSchema() {
        return productSchema;
    }

    public void setProductSchema(List<String> productSchema) {
        this.productSchema = productSchema;
    }

    public List<ProductSearchProductPropertyModel> getProductProperties() {
        return productProperties;
    }

    public void setProductProperties(List<ProductSearchProductPropertyModel> productProperties) {
        this.productProperties = productProperties;
    }

    public ProductSearchCompositionInfoModel getCompositionInformation() {
        return compositionInformation;
    }

    public void setCompositionInformation(ProductSearchCompositionInfoModel compositionInformation) {
        this.compositionInformation = compositionInformation;
    }

    public List<Object> getParentKits() {
        return parentKits;
    }

    public void setParentKits(List<Object> parentKits) {
        this.parentKits = parentKits;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}