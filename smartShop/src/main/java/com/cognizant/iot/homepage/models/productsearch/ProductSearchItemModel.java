package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/21/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductSearchItemModel implements Serializable {

    @SerializedName("Url")
    @Expose
    private String url;
    @SerializedName("AltText")
    @Expose
    private String altText;
    @SerializedName("IsSelfHosted")
    @Expose
    private Boolean isSelfHosted;
    @SerializedName("IsDefault")
    @Expose
    private Boolean isDefault;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAltText() {
        return altText;
    }

    public void setAltText(String altText) {
        this.altText = altText;
    }

    public Boolean getIsSelfHosted() {
        return isSelfHosted;
    }

    public void setIsSelfHosted(Boolean isSelfHosted) {
        this.isSelfHosted = isSelfHosted;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

}