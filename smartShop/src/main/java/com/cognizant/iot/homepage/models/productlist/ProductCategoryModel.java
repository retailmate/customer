package com.cognizant.iot.homepage.models.productlist;

/**
 * Created by 452781 on 2/21/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductCategoryModel {

    @SerializedName("@odata.context")
    @Expose
    private String odataContext;
    @SerializedName("value")
    @Expose
    private List<ProductCategoryValueModel> value = null;

    public String getOdataContext() {
        return odataContext;
    }

    public void setOdataContext(String odataContext) {
        this.odataContext = odataContext;
    }

    public List<ProductCategoryValueModel> getValue() {
        return value;
    }

    public void setValue(List<ProductCategoryValueModel> value) {
        this.value = value;
    }

}